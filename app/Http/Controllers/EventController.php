<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    //

    public function store(Request $r)
    {
        if ($r->input('type') === 'deposit') {
            return $this->deposit(
                $r->input('destination'),
                $r->input('amount')
            );
        } elseif ($r->input('type') === 'withdraw') {
            return $this->withdraw(
                $r->input('origin'),
                $r->input('amount')
            );
        } elseif ($r->input('type') === 'transfer') {
            return $this->transfer(
                $r->input('origin'),
                $r->input('destination'),
                $r->input('amount')
            );
        }
    }

    private function deposit($destination, $amount)
    {
        $acc = Account::firstOrCreate([
            'id'    => $destination
        ]);

        $acc->balance += $amount;
        $acc->save(); //    UPDATE

        return response()->json([
            "destination" => [
                'id' => $destination,
                'balance' => $acc->balance
            ]
        ], 201);
    }

    private function withdraw($origin, $amount)
    {
        $acc = Account::findOrFail($origin);

        $acc->balance -= $amount;
        $acc->save(); //    UPDATE

        return response()->json([
            "origin" => [
                'id' => $acc->id,
                'balance' => $acc->balance
            ]
        ], 200);
    }

    private function transfer($origin, $destination, $amount)
    {
        $accountOrigin = Account::findOrFail($origin);
        $accountDestination = Account::findOrFail($destination);

        DB::transaction(function () use ($accountOrigin, $accountDestination, $amount) {
            $accountOrigin->balance -= $amount;
            $accountDestination->balance += $amount;
            $accountDestination->save(); //    UPDATE
            $accountOrigin->save(); //    UPDATE 
        });

        return response()->json([
            "origin" => [
                'id' => $accountOrigin->id,
                'balance' => $accountOrigin->balance
            ],
            "destination" => [
                'id' => $accountDestination->id,
                'balance' => $accountDestination->balance
            ]
        ], 200);
    }
}
