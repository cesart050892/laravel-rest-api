<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    //
    public function show(Request $r){
        $id = $r->input('account_id');
        $acc = Account::findOrfail($id);
        return response()->json([
            'balance' => $acc->balance
        ]);
    }
}
