

[Agrega tus propias clases en laravel](https://programacionymas.com/blog/agregar-clases-propias-laravel)

```
"psr-4": {
    "App\\": "app/",
    "Libraries\\": "libraries/"
}

composer dump-autoload

php artisan tinker

Psy Shell v0.11.2 (PHP 8.1.3 — cli) by Justin Hileman
>>> use Libraries\Manager
>>> $m = new Manager()
=> Libraries\Manager {#3501}
>>> $m->dataResponse('Hi')
=> Illuminate\Http\JsonResponse {#3517
     +headers: Symfony\Component\HttpFoundation\ResponseHeaderBag {#3518},
     +original: [
       "content" => "Hi",
     ],
     +exception: null,
   }


```