<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Account;

class AccountManagmentTest extends TestCase
{
    /** @test */
    public function reset_state_before_start()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/reset');

        $response->assertOk();
    }

    /** @test */
    public function get_balance_for_non_existing_account()
    {
        // $this->withoutExceptionHandling();

        $response = $this->get('api/balance?account_id=1234');

        $response->assertStatus(404);
        $response->assertJson([
            'message' => "Record not found.",
        ]);
    }

    // /** @test */
    public function create_account_with_initial_balance()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('api/event', [
            'type'          => 'deposit',
            'destination'   => 100,
            'amount'        => 10
        ]);

        $response->assertStatus(201);
    }

    /** @test */
    public function get_balance_for_existing_account()
    {
        // $this->withoutExceptionHandling();

        $response = $this->get('api/balance?account_id=100');

        $response->assertOk();
        $response->assertJson([
            'balance'   => 100,
        ]);
    }


    public function test_count()
    {
        $this->withoutExceptionHandling();

        $this->assertCount(1, Account::all());
    }


    public function a_equals()
    {
        $this->withoutExceptionHandling();

        $acc = Account::first();

        $this->assertEquals($acc->id, 1234);
        $this->assertEquals($acc->balance, 100);
    }
}
