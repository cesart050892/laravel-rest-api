<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ResetController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Description
// Method URI Example data
// Status code Message status
Route::post('/reset', [ResetController::class, 'reset']);

Route::get('/balance', [BalanceController::class, 'show']);

Route::post('/event', [EventController::class, 'store']);


